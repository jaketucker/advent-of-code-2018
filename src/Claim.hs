module Claim
    ( countOverlaps
    , findUnique
    ) where

import           Data.List.Unique             (repeated)
import           Data.Maybe                   (catMaybes)
import           ParserUtils                  (digits)
import           Text.ParserCombinators.ReadP (ReadP, char, eof, readP_to_S, string)

-- Task 5
data Claim = Claim {
    id     :: Int,
    x      :: Int,
    y      :: Int,
    width  :: Int,
    height :: Int
} deriving (Show, Eq)

countOverlaps :: String -> Int
countOverlaps = length . repeated . foldl (++) [] . map findIndexes . parseClaims

findIndexes :: Claim -> [(Int, Int)]
findIndexes claim = [(x,y) | x <- [startX .. endX]
                           , y <- [startY .. endY]]
  where
    startX  = x claim
    endX    = startX + width claim - 1
    startY  = y claim
    endY    = startY + height claim - 1

parseClaims :: String -> [Claim]
parseClaims = catMaybes . map parseClaim . lines

parseClaim :: String -> Maybe Claim
parseClaim claimString
  | length parsed > 0 = Just $ fst $ head parsed
  | otherwise         = Nothing
  where
    parsed = (readP_to_S claimParser) claimString

claimParser :: ReadP Claim
claimParser = do
    char '#'
    id <- digits

    string " @ "

    x <- digits
    char ','
    y <- digits

    string ": "

    width <- digits
    char 'x'
    height <- digits

    eof

    return $ Claim id x y width height

-- Task 6
findUnique :: String -> Int
findUnique = Claim.id . findUnique' . parseClaims

findUnique' :: [Claim] -> Claim
findUnique' claims = findUniqueRecurse claims (tail claims) (head claims)

findUniqueRecurse :: [Claim] -> [Claim] -> Claim -> Claim
findUniqueRecurse allClaims remainingClaims current
  | currentIsUnique         = current
  | otherwise               = findUniqueRecurse allClaims (tail remainingClaims) (head remainingClaims)
  where
    claimsToCompare = (current /=) `filter` allClaims
    currentIsUnique = not $ any (claimsOverlap current) claimsToCompare

claimsOverlap :: Claim -> Claim -> Bool
claimsOverlap c1 c2 = (left1 <= right2) && (right1 >= left2)
                   && (top1 >= bottom2) && (bottom1 <= top2)
  where
    left1   = x c1
    right1  = left1 + (width c1) - 1
    top1    = bottom1 + (height c1) - 1
    bottom1 = y c1

    left2   = x c2
    right2  = left2 + (width c2) - 1
    top2    = bottom2 + (height c2) - 1
    bottom2 = y c2