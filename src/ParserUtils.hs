module ParserUtils
    ( digits
    ) where

import           Control.Monad                (liftM)
import           Text.ParserCombinators.ReadP (ReadP, many1, readP_to_S, satisfy)

isDigit :: Char -> Bool
isDigit c = any (c ==) "1234567890"

digitChars :: ReadP [Char]
digitChars = many1 $ satisfy isDigit

digits :: ReadP Int
digits = (liftM read) digitChars
