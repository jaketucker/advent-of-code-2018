module Shift where

import           Control.Applicative          ((<|>))
import           Data.Maybe                   (catMaybes)
import           ParserUtils                  (digits)
import           Text.ParserCombinators.ReadP (ReadP, char, eof, readP_to_S, string)

data LogEntry = LogEntry {
  timestamp  :: Timestamp,
  event      :: Event
} deriving (Show)

data Timestamp = Timestamp {
  year   :: Int,
  month  :: Int,
  day    :: Int,
  hour   :: Int,
  minute :: Int
} deriving (Show)

data Event = Wake
           | Sleep
           | StartShift (Int)
             deriving (Show)

parseLogs :: String -> [LogEntry]
parseLogs = catMaybes . map parseLog . lines

parseLog :: String -> Maybe LogEntry
parseLog logString
  | length parsed > 0 = Just $ fst $ head parsed
  | otherwise         = Nothing
  where
    parsed = (readP_to_S logParser) logString

logParser :: ReadP LogEntry
logParser = do
  char '['
  timestamp <- timestampParser
  char ']'

  char ' '

  event <- eventParser

  eof

  return $ LogEntry timestamp event

timestampParser :: ReadP Timestamp
timestampParser = do
  year <- digits
  char '-'
  month <- digits
  char '-'
  day <- digits

  char ' '

  hour <- digits
  char ':'
  minute <- digits

  return $ Timestamp year month day hour minute

eventParser :: ReadP Event
eventParser = wake <|> sleep <|> startShift
  where
    wake  = string "wakes up" >> return Wake
    sleep = string "falls asleep" >> return Sleep
    startShift = do
      string "Guard #"
      guardId <- digits
      string " begins shift"
      return $ StartShift guardId
