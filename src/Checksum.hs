module Checksum
    ( calcChecksum
    , findSimilar
    ) where

import           Data.Maybe (catMaybes)

-- Day 2, task 1

calcChecksum :: String -> Int
calcChecksum = multiplyTuple . sumTuples . map findRepeats . lines

multiplyTuple :: (Int, Int) -> Int
multiplyTuple (first, second) = first * second

sumTuples :: [(Int, Int)] -> (Int, Int)
sumTuples = foldl applyTuple (0, 0)

applyTuple :: (Int, Int) -> (Int, Int) -> (Int, Int)
applyTuple accumulated next = (fst accumulated + fst next, snd accumulated + snd next)

findRepeats :: String -> (Int, Int)
findRepeats boxId = (fromEnum contains2, fromEnum contains3)
  where
    countChars c = length $ filter (c ==) boxId
    charCounts   = map countChars boxId
    contains2    = 2 `elem` charCounts
    contains3    = 3 `elem` charCounts

-- Day 2, task 2

findSimilar :: String -> Maybe String
findSimilar input = do
    (s1, s2) <- findMatch $ lines input
    return $ removeDifferingChars s1 s2

findMatch :: [String] -> Maybe (String, String)
findMatch [] = Nothing
findMatch (searchId:remainingIds)
  | foundMatch = Just (searchId, head matches)
  | otherwise  = findMatch remainingIds
  where
    matches    = filter (isSimilar searchId) remainingIds
    foundMatch = length matches > 0

isSimilar :: String -> String -> Bool
isSimilar s1 s2 = lengthsEqual && areSimilar
  where
    s1Length     = length s1
    lengthsEqual = s1Length == length s2
    areSimilar   = countSameChars s1 s2 == s1Length - 1

countSameChars :: String -> String -> Int
countSameChars [] [] = 0
countSameChars (c1:remaining1) (c2:remaining2) = fromEnum (c1 == c2) + countSameChars remaining1 remaining2

removeDifferingChars :: String -> String -> String
removeDifferingChars s1 s2 = catMaybes groupedChars
  where
    matchChars c1 c2
      | c1 == c2  = Just c1
      | otherwise = Nothing
    groupedChars = zipWith matchChars s1 s2
