module FrequencyChange
    ( FrequencyChange
    , applyChange
    , parseChanges
    ) where

import           Data.Maybe (catMaybes)
import           Text.Read  (readMaybe)

data FrequencyChange = Add Int
                     | Subtract Int

applyChange :: Int -> FrequencyChange -> Int
applyChange currentFrequency (Add amount)      = currentFrequency + amount
applyChange currentFrequency (Subtract amount) = currentFrequency - amount

parseChanges :: String -> [FrequencyChange]
parseChanges = catMaybes . map parseChange . lines

parseChange :: String -> Maybe FrequencyChange
parseChange "" = Nothing
parseChange (changeType:amountString) = do
  amount <- readMaybe amountString
  case changeType of
    '+' -> Just $ Add amount
    '-' -> Just $ Subtract amount
    _   -> Nothing
