module Frequency
    ( addFrequencies
    , findFirstRepeat
    ) where

import           Data.IntSet     (IntSet, insert, member, singleton)
import           FrequencyChange (FrequencyChange, applyChange, parseChanges)

addFrequencies :: String -> Int
addFrequencies = foldl applyChange initialFrequency . parseChanges
  where initialFrequency = 0

findFirstRepeat :: String -> Int
findFirstRepeat input = findFirstRepeat' initialHistory initialFrequency $ cycle changes
  where
    initialHistory   = singleton 0
    initialFrequency = 0
    changes          = parseChanges input

findFirstRepeat' :: IntSet -> Int -> [FrequencyChange] -> Int
findFirstRepeat' history currentFrequency changes
  | nextFrequency `member` history = nextFrequency
  | otherwise                      = findFirstRepeat' updatedHistory nextFrequency remainingChanges
  where
    nextChange       = head changes
    remainingChanges = tail changes
    nextFrequency    = currentFrequency `applyChange` nextChange
    updatedHistory   = nextFrequency `insert` history
