module Main where

import           Checksum           (calcChecksum, findSimilar)
import           Claim              (countOverlaps, findUnique)
import           Control.Exception  (IOException, try)
import           Frequency          (addFrequencies, findFirstRepeat)
import           System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  runTask args

runTask :: [String] -> IO ()
runTask [] = putStrLn "No task given"
runTask (task:args) = do
  possibleInput <- (try :: IO String -> IO (Either IOException String)) $
                      readFile $ "puzzleinput/" ++ task ++ ".txt"

  case possibleInput of
    Left exception -> putStrLn "Failed to read input data for task"
    Right input -> case task of
      "1"       -> run addFrequencies input
      "2"       -> run findFirstRepeat input
      "3"       -> run calcChecksum input
      "4"       -> run findSimilar input
      "5"       -> run countOverlaps input
      "6"       -> run findUnique input
      otherwise -> putStrLn $ "Task \"" ++ task ++ "\" not found"
  where run f = putStrLn . show . f
